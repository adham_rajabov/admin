admin generator
===============
this will generate admin

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist adham/yii2-admin "@dev"
```

or add

```
"adham/yii2-admin": "@dev"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \adham\admin\AutoloadExample::widget(); ?>```