<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var yii\web\View $this
 * @var app\models\Checking $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="checking-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
    
            <?php
                if (!empty($fields)) {

                    foreach ($fields as $field) {

                        echo yii\base\View::render('@vendor/admin/yii2-admin/views/html/'.$field['type'].'.php', [
                            'model' => $model,
                            'form' => $form,
                            'field' => $field,
                        ]);

                    }

                }
            ?>

        </div>

    <div class="form-group">

        <?php if(!(isset($options['submitButton']))) $options['submitButton'] = true;

        if (!(isset($options['submitButton']) && $options['submitButton']==false)) : ?>
            <?= Html::submitButton($model->isNewRecord ? (is_array($options['submitButton'])?$options['submitButton'][0]:'Қўшиш') : (is_array($options['submitButton'])?$options['submitButton'][1]:'Янгилаш'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if (isset($options['backButton'])) : ?>
            <?= Html::button($options['backButton']['title'], ['class' => 'btn btn-default', 'onclick' => 'location.href="'.$options['backButton']['url'].'";return false;']) ?>
        <?php endif; ?>

    </div>

    <?php ActiveForm::end(); ?>

    <?php 
        if (isset($options['js'])) {
            $this->registerJs($options['js']);
        }
    ?>

</div>