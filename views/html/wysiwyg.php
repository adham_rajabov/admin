<?= $form->field($model, $field['name'])->widget(\dosamigos\ckeditor\CKEditor::className(), [
        'options' => isset($field['options'])?$field['options']:['rows' => 6],
        'preset' => 'basic',
        'clientOptions' => isset($field['clientOptions'])?$field['clientOptions']:[],
    ]) ?>

