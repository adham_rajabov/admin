<div class="manyToMany">
	<?php 

		// get many to many model
		$manyToMany = new $field['param']['manyToMany'];

		// get items to show
		$items = $field['param']['items'];

		// get id name of a main model
		$mainIdName = $model->tableSchema->primaryKey;
		$mainIdName = $mainIdName[0];

		// get firstId and secondId
		$firstId = $field['param']['firstId'];
		$secondId = $field['param']['secondId'];

		// get selected datas to show selected
		$selectedDatas = array();
		if ($model->$mainIdName>0) {
			$selectedDatas = \yii\helpers\ArrayHelper::getColumn($field['param']['manyToMany']::find()->where($firstId.'='.$model->$mainIdName)->all(), $secondId);
		}

		// pr($field['param']['items']);
	?>

	<div class="form-group required has-success">
		
		<?php if (isset($field['title'])) : ?>
			<label class="control-label"><?php echo $field['title']; ?></label>
		<?php endif; ?>

		<?php 

			echo \kartik\widgets\Select2::widget([
			    'name' => $field['name'], 
			    'data' => $field['param']['items'],
			    'options' => isset($field['options'])?$field['options']:[],
			]);		

		?>

		<div class="help-block"></div>
	</div>

	<?php

	// if related model exist
	if (isset($field['related'])) {

		ob_start(); ?>

		    $(function() {

				// reload related page loading
		        $("#<?php echo $field['related']['relatedId']; ?>").change(function() {
				
					$('.manyToMany').load("<?php echo $field['related']['action']; ?>", {
						modelId: <?php echo ($model->$mainIdName>0)?$model->$mainIdName:0; ?>,
						relatedId: $("#<?php echo $field['related']['relatedId']; ?> option:selected").val()
					}, function() {
						$("#<?php echo $field['options']['id']; ?>").val([<?php echo isset($field['param']['selected'])?implode($field['param']['selected'], ','):''; ?>]).select2();
					});
		    	});

				// if autoload true, reload related in page loading
		    	<?php if (isset($field['related']['autoload']) && $field['related']['autoload']==true) : ?>
		    		$("#<?php echo $field['related']['relatedId']; ?>").change();
				<?php endif; ?>		    	
		    
		    });

		<?php $script = ob_get_clean();


		$this->registerJs($script);

	} else {

		ob_start(); ?>

		    $(function() {
		    	$('#<?php echo $field['options']['id']; ?>').val([<?php echo !empty($selectedDatas)?implode($selectedDatas, ','):''; ?>]).select2();
			});

		<?php $script = ob_get_clean();


		$this->registerJs($script);

	}
	?>
</div>