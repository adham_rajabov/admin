<div class="oneToMany">
	<?php 

		// get many to many model
		$oneToMany = new $field['param']['oneToMany'];

		// get fields to show
		$myFields = $field['param']['fields'];

		// get id name of a main model
		$mainIdName = $model->tableSchema->primaryKey;
		$mainIdName = $mainIdName[0];

		// get relatedId
		$relatedId = $field['param']['relatedId'];

		$idOneToMany = $field['options']['id'];

		// get old datas
		$oldValues = [];
		if (!$model->isNewRecord) {
			$oldValues = $field['param']['oneToMany']::find()->where($field['param']['relatedId'].'='.$model->$mainIdName)->all();
		}

		// pr($field['param']['items']);
	?>

	<div class="form-group required has-success" style="border:1px solid #ddd;padding:10px" id="<?php echo $idOneToMany; ?>">

		<button type="button" class="btn btn-default btn-xs addMoreGroup">
  			<span class="glyphicon glyphicon-plus"></span>
		</button>
		<div class="div"></div>		

		<div class="oneToManyBlock" style="display:none;">
			<div class="form-group required has-success" style="border:1px solid #ddd;padding:10px;">
			
				<?php if (isset($field['title']) && !empty($field['title'])) : ?>
					<label class="control-label"><?php echo $field['title']; ?></label>
				<?php endif; ?>

		        <?php 
		            if (!empty($myFields)) {

		                foreach ($myFields as $myField) {
						
		                    echo yii\base\View::render('@vendor/admin/yii2-admin/views/html/'.$myField['type'].'.php', [
		                        'model' => $oneToMany,
		                        'form' => $form,
		                        'field' => $myField,
		                    ]);
		                    
		                }

		            }
		        ?>

				<button type="button" class="btn btn-default btn-xs removeMoreGroup">
		  			<span class="glyphicon glyphicon-minus"></span>
				</button>
				
		    </div>
		</div>

		<?php if (!empty($oldValues)) : ?>

			<?php foreach ($oldValues as $oldValue) :?>

				<div class="form-group required has-success" style="border:1px solid #ddd;padding:10px;">
				
					<?php if (isset($field['title'])) : ?>
						<label class="control-label"><?php echo $field['title']; ?></label>
					<?php endif; ?>

			        <?php 
			            if (!empty($myFields)) {

			                foreach ($myFields as $myField) {
							
			                    echo yii\base\View::render('@vendor/admin/yii2-admin/views/html/'.$myField['type'].'.php', [
			                        'model' => $oldValue,
			                        'form' => $form,
			                        'field' => $myField,
			                    ]);
			                    
			                }

			            }
			        ?>

					<button type="button" class="btn btn-default btn-xs removeMoreGroup">
			  			<span class="glyphicon glyphicon-minus"></span>
					</button>
					
			    </div>

			<?php endforeach; ?>

		<?php endif; ?>

		<div class="help-block"></div>
	</div>

</div>

<?php
		ob_start(); ?>

		    $(function() {

		    	$('.addMoreGroup').click(function() {

					var mainDivId = $(this).parent().attr('id');

					var block = $('#'+mainDivId + ' .oneToManyBlock').html();

					$('#'+mainDivId).append(block);

		    	});

		    	$('.addMoreGroup').click();

		    	$('#<?php echo $idOneToMany; ?>').on('click', '.removeMoreGroup', function() {

					$(this).parent().remove();

		    	});

		    });

		<?php $script = ob_get_clean();

		$this->registerJs($script);
?>