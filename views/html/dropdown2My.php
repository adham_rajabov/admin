<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

    echo \kartik\widgets\Select2::widget([
        'name' => $field['name'],
        'value' => isset($field['selection'])?$field['selection']:null,
        'data' => $field['items'],
        'options' => isset($field['options'])?$field['options']:[]
    ]);

    ?>
</div>
