
<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

echo \extead\autonumeric\AutoNumeric::widget([
    'name' => $field['name'],
    'value' => $field['value']
]);?>

<br>