<?php

echo \kartik\widgets\DateTimePicker::widget([
    'model' => $model,
    'form' => $form,
    'options' => ['placeholder1' => 'Select operating time ...', 'style'=>'background:none; color:#CBBAAF;', 'id'=>'event-datetime'],
    'removeButton' => false,
    'pickerButton' => ['icon' => 'time'],
    'pluginOptions' => [
        'todayHighlight' => true,
        'autoclose' => true
    ]
])

?>