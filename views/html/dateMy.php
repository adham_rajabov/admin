<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

    echo \kartik\widgets\DatePicker::widget([
        'name' => $field['name'],
        'value' => isset($field['value'])?$field['value']:'',
        'options' => isset($field['options'])?$field['options']:[],
        'pluginOptions' => isset($field['pluginOptions'])?$field['pluginOptions']:[],
    ]);

    ?>
</div>