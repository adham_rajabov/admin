<?php 

echo $form->field($model, $field['name'])->widget(\kartik\widgets\Select2::classname(), [
    // 'data' => array_merge(["" => ""], $data),
    'data' => $field['items'],
    'options' => isset($field['options'])?$field['options']:[],
]);

?>
