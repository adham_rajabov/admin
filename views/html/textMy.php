<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

    if(!isset($field['options']['type']))
        $field['options']['type'] = 'text';

    echo \yii\helpers\Html::input($field['options']['type'],
        $field['name'],
        isset($field['value'])?$field['value']:null,
        isset($field['options'])?$field['options']:['class' => 'form-control']
    );

    ?>
</div>

